module TimeCalculator where

import Data.Maybe
import Data.Fixed (div', mod')
import Control.Monad (unless)
import System.Environment
import Text.Printf (printf)
import Lexer
import TimeParser

{-todo
allow format 5d2s, 17:30m
history by up-arrow
`now` keyword
functions like days()
flags like natural-language-output

-}

main :: IO ()
main = do
  args <- getArgs
  case args of
    [] -> putStrLn "This is time-calc. Enter 'q' to exit." >> loop
    _  -> mapM_ (putStrLn . compute) args

loop :: IO ()    
loop = do
  line <- getLine
  unless (line `elem` ["", "q", ":q", "quit", "exit"]) $ do
    putStrLn (compute line) -- process line
    loop

computeIO :: IO ()
computeIO = do
  inputString <- getLine
  putStrLn (compute inputString)
  
compute :: String -> String
compute string = maybe "" (showTimePretty . eval) (parseTime =<< lexString string) -- TODO try to make point-free
  
{-compute string = do
  let lexed  = lexString string
  let parsed = parseTime =<< lexed
  let evaled = eval <$> parsed :: (Maybe Double)
  let bb0 = showTimePretty <$> evaled :: (Maybe String)
  let pe = fromMaybe "" bb0
  pe-}
  
-------Eval

eval :: Expr -> Double
eval (DoubleLit i) = i
eval (Add  a b) = eval a + eval b
eval (Sub  a b) = eval a - eval b
eval (Mult a b) = eval a * eval b
eval (Div  a b) = eval a / eval b
eval (Expo a b) = eval a ** eval b
eval (Neg  a  ) = -(eval a)

showTimePretty :: Double -> String
showTimePretty t | t<60      = stripTrailing0s $ show t
                 | otherwise = sign ++ dropWhile (=='0') (concat values)
  where
    seconds = stripTrailing0s (printf "%06.3f" s)
    values  = reverse $ seconds : zipWith printf ["%02d:","%02d:","%d-"] (reverse $ dropWhile (==0) [d,h,m])
    sign = if t >= 0 then ""
                     else "-"
    ([d,h,m],s) =  segment (abs t)


showTime :: Double -> String
showTime t | t >= 0    = printf "%d-%02d:%02d:%06.3f" d h m s -- :: String
           | otherwise = printf "-%d-%02d:%02d:%06.3f" d h m s
 where
  ([d,h,m],s) =  segment (abs t)


--segments an amount of seconds into [days, hours, minutes, seconds]
segment :: Double -> ([Int], Double)
segment t = (map round [a,b,c],d)
  where
    (a,[b,c,d]) = foldr f (t, []) [24,60,60]
         where 
            f m (remaining, s) = (fromIntegral (div' remaining m), mod' remaining m:s)

stripTrailing0s :: String -> String
stripTrailing0s = rmDot . rstrip
  where
    rstrip = reverse . dropWhile (=='0') . reverse
    rmDot s | last s == '.' = init s
            | otherwise     = s
            
