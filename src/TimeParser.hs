{-# LANGUAGE LambdaCase #-}

module TimeParser ( parseTime
                  , Expr(..)
                  ) where


import Control.Applicative
import Prelude hiding (div) -- because we have our own div
import Lexer

data Expr = DoubleLit Double    -- double constants, leaves of the expression tree
          | Add    Expr Expr    -- addition node
          | Sub    Expr Expr
          | Mult   Expr Expr    -- multiplication node
          | Div    Expr Expr
          | Expo   Expr Expr
          | Neg    Expr
          deriving (Show, Eq)

newtype Parser a = Parser { parse :: [Token] -> Maybe (a, [Token])}

instance Functor Parser where
  fmap f p = Parser $ \s ->
    case parse p s of
      Nothing -> Nothing
      Just (v,s') -> Just (f v,s') -- Todo can we simply do `(first f) <$> parse p s` ?
                                   -- import Data.Arrow

instance Applicative Parser where
  pure a = Parser $ \s -> Just (a,s)
  f <*> a = Parser $ \s ->  -- f :: Parser (a -> b), a :: Parser a
    case parse f s of
      Nothing -> Nothing
      Just (g,s') -> parse (fmap g a) s' -- g :: a -> b, fmap g a :: Parser b

instance Alternative Parser where
  empty = Parser $ const Nothing
  p1 <|> p2 = Parser $ \s ->
    case parse p1 s of
      Just (a,s') -> Just (a,s')
      Nothing -> parse p2 s

-- http://www.cse.chalmers.se/edu/year/2017/course/TDA452/lectures/Parsing.html
instance Monad Parser where
  return x = Parser (\ s -> Just (x,s)) -- Todo use pure
  Parser p >>= f = Parser (\ s -> case p s of
                          Nothing    -> Nothing
                          Just (a,r) -> parse (f a) r)


token :: Token -> Parser Token
token t = Parser $ \case
    t':s | t == t' -> Just (t, s) -- Todo we have negation, use it!
    _ -> Nothing


double :: Parser Double
double = Parser $ \case
    TimeT i:s -> Just (i, s)
    _ -> Nothing 

plus :: Parser Token 
plus  = token PlusT 
minus = token MinusT 
times = token TimesT 
div   = token DivT 
expo  = token ExpoT 
openP  = token OpenT
closeP = token CloseT


parseTime :: [Token] -> Maybe Expr
parseTime tokens =
  case parse cE tokens of
    Just (expr, []) -> Just expr    
    _               -> Nothing


cE :: Parser Expr
cE = do
  t <- cT :: Parser Expr
  additional <- many ((,) <$> (plus <|> minus) <*> cT)  :: Parser [(Token, Expr)]
  let cascade = foldl op t additional :: Expr
            where
                op :: Expr -> (Token, Expr) -> Expr
                op a (pm, b) = case pm of
                  PlusT  -> Add a b
                  MinusT -> Sub a b
  return cascade


cT :: Parser Expr
cT = do
  f <- cF :: Parser Expr
  additional <- many ((,) <$> (times <|> div) <*> cF) :: Parser [(Token, Expr)]
  let cascade = foldl op f additional :: Expr
            where
                op a (pm, b) = case pm of
                  TimesT -> Mult a b
                  DivT   -> Div  a b
  return cascade


cF :: Parser Expr
cF = do
  f <- cP
  opti <- optional ((,) <$> expo <*> cF)
  let foo = case opti of
               Just (ExpoT, exponent) -> Expo f exponent
               Nothing -> f
  return foo


--P for parenthesis
cP :: Parser Expr
cP =     DoubleLit <$> double
     <|> (openP *> cE <* closeP)
     <|> Neg <$> (minus *> cT)

