module Lexer ( lexString
             , Token(..)
             , toTokenList  -- for test
             ) where

import Data.Maybe
import Text.Trifecta 
import Control.Applicative

data Token = TimeT Double
           | PlusT
           | MinusT
           | TimesT
           | DivT
           | ExpoT
           | OpenT
           | CloseT
           deriving (Eq, Show)

toTokenList :: String -> [Token]
toTokenList = fromMaybe [] . lexString

lexTimeToken :: String -> Result Token
lexTimeToken = parseString timeParser mempty

lexString :: String -> Maybe [Token]
lexString = maybefy . parseString p mempty
 where
   p :: Parser [Token]
   p = do
     skipMany (char ' ')
     many oneStep
     
oneStep :: Parser Token
oneStep = do
  tmp <-       timeParser
           <|> plusParser
           <|> minusParser
           <|> timesParser
           <|> divParser
           <|> powerParser
           <|> openCParser
           <|> closeCParser
  skipMany (char ' ')
  return tmp

-- Glue --
maybefy :: Result a -> Maybe a
maybefy (Success a) = Just a
maybefy (Failure _) = Nothing

-- Parsing

timeParser :: Parser Token
timeParser = do
  time <- try timeDaysHoursMinutesSeconds
          <|> (try timeHoursMinutesSeconds
               <|> (try timeMinutesSeconds
                    <|> (try timeSingle
                         <|> timeSeconds)))

  return (TimeT time)


timeDaysHoursMinutesSeconds :: Parser Double
timeDaysHoursMinutesSeconds = (\days hms -> fromIntegral days * 24 * 3600 + hms)      <$> digits <* char '-' <*> timeHoursMinutesSeconds

timeHoursMinutesSeconds :: Parser Double
timeHoursMinutesSeconds =     (\hours ms -> fromIntegral hours * 3600 + ms)           <$> digits <* char ':' <*> timeMinutesSeconds

timeMinutesSeconds :: Parser Double
timeMinutesSeconds =          (\minutes seconds -> fromIntegral minutes*60 + seconds) <$> digits <* char ':' <*> timeSeconds

timeSingle :: Parser Double
timeSingle = do
  time <- timeSeconds
  dhms <- oneOf "dhms"
  let timeInSeconds = case dhms of 
        'd' -> time * 24 * 60 * 60
        'h' -> time      * 60 * 60
        'm' -> time           * 60
        's' -> time
        -- -Wall wants me to add a default case, but it would be unreachable, how to tell the type system?
        -- Todo lambdacase?
  return timeInSeconds

digits :: Parser Int -- natural accepts trailing whitespace so `natural >> char '-'` would succeed on `123 -` so we build our own
digits = fromInteger <$> decimal
  
--digits = (\x -> read x :: Int) <$> some digit

timeSeconds :: Parser Double
timeSeconds = doubleParser


--Parses [0-9]+(.[0-9]+)? and returns Double
doubleParser :: Parser Double
doubleParser = (read ... (++)) <$> some digit <*> option "" ((:) <$> char '.' <*> some digit)
(...) = (.) . (.)



plusParser :: Parser Token
plusParser  = char '+' >> return PlusT

minusParser = char '-' >> return MinusT     :: Parser Token

timesParser = char '*' >> return TimesT     :: Parser Token

divParser   = char '/' >> return DivT       :: Parser Token

powerParser = char '^' >> return ExpoT      :: Parser Token

openCParser = char '(' >> return OpenT      :: Parser Token

closeCParser =char ')' >> return CloseT     :: Parser Token
