module Main where
 
import Test.Hspec
import TimeCalculator hiding (main)
import Lexer
import TimeParser

main :: IO ()
main = hspec $ do 
  describe "simple time terms" $ do
    it "toTokenList should transform string of one time-term to a time token" $ do
      toTokenList "1s" `shouldBe` [TimeT     1]
      toTokenList "1m" `shouldBe` [TimeT    60]
      toTokenList "1h" `shouldBe` [TimeT  3600]
      toTokenList "1d" `shouldBe` [TimeT 86400]
      toTokenList         "1" `shouldBe` [TimeT     1]
      toTokenList        "01" `shouldBe` [TimeT     1]
      toTokenList       "001" `shouldBe` [TimeT     1]
      toTokenList      "1:01" `shouldBe` [TimeT    61]
      toTokenList       "1:1" `shouldBe` [TimeT    61]
      toTokenList     "01:01" `shouldBe` [TimeT    61]
      toTokenList    "001:01" `shouldBe` [TimeT    61]
      toTokenList   "001:001" `shouldBe` [TimeT    61]
      toTokenList   "1:01:01" `shouldBe` [TimeT  3661]
      toTokenList "1-1:01:01" `shouldBe` [TimeT 90061]
      
  describe "simple time terms" $ do
    it "toTokenList should transform `Xm` string to TimeT-token with correct amount of secs" $ do
      toTokenList "1m" `shouldBe` [TimeT  60]
      toTokenList "2m" `shouldBe` [TimeT 120]
      toTokenList "5m" `shouldBe` [TimeT 300]
  describe "Lexer transforms every non-time symbol to its corresponding token" $ do
    it "single" $ do
      toTokenList "+" `shouldBe` [PlusT]
      toTokenList "-" `shouldBe` [MinusT]
      toTokenList "*" `shouldBe` [TimesT]
      toTokenList "/" `shouldBe` [DivT]
      toTokenList "^" `shouldBe` [ExpoT]
      toTokenList "(" `shouldBe` [OpenT]
      toTokenList ")" `shouldBe` [CloseT]
    it "several" $ do
      toTokenList "+ - * / ^ ( )" `shouldBe` [PlusT, MinusT, TimesT, DivT, ExpoT, OpenT, CloseT]
  describe "simple time terms" $ do
    it "toTokenList should transform string of simple arithmetic to list of Tokens" $ do
      toTokenList "1m+1m" `shouldBe` [TimeT 60,PlusT, TimeT 60]
      toTokenList "1m-1m" `shouldBe` [TimeT 60,MinusT,TimeT 60]
      toTokenList "1m*1m" `shouldBe` [TimeT 60,TimesT, TimeT 60]
  describe "minus" $ do
    it "`-` binds days to h:m:s if there is no space" $ do
      toTokenList "1-00:00:01" `shouldBe` [TimeT 86401]
    it "`-` is minus if space separates it from at least one surrounding token" $ do
      toTokenList "1 - 00:00:01" `shouldBe` [TimeT 1, MinusT, TimeT 1]
      toTokenList  "1- 00:00:01" `shouldBe` [TimeT 1, MinusT, TimeT 1]
      toTokenList "1 -00:00:01"  `shouldBe` [TimeT 1, MinusT, TimeT 1]

  describe "simple time terms" $ do
    it "toTokenList should transform string to list of Tokens" $ do
      parseTime [TimeT 60,PlusT,  TimeT 60] `shouldBe` (Just (Add (DoubleLit 60) (DoubleLit 60)))
      parseTime [TimeT 60,MinusT, TimeT 60] `shouldBe` Just (Sub (DoubleLit 60) (DoubleLit 60))
      parseTime [TimeT 60,TimesT, TimeT 60] `shouldBe` Just (Mult (DoubleLit 60) (DoubleLit 60))
  describe "string to string" $ do
    it "toTokenList should transform string to list of Tokens" $ do
      compute "5-20:11:02 - 1-15:03:01" `shouldBe` "4-05:08:01"
      compute "1m - 1m"          `shouldBe` "0"
      toTokenList "1h - 1m - 1s" `shouldBe` [TimeT 3600,MinusT,TimeT 60,MinusT,TimeT 1]
      
    it "groups subtractions left-associative" $ do      
      parseTime [TimeT 3,MinusT,TimeT 2,MinusT,TimeT 1] `shouldBe` Just (Sub (Sub (DoubleLit 3)
                                                                                    (DoubleLit 2))
                                                                               (DoubleLit 1))
      compute     "1h - 1m - 1s" `shouldBe` "58:59"
      compute "1h - 1m + 1s"  `shouldBe` "59:01"
      compute "2-22:11:02 - 1-21:10:01 * 0 - 1m" `shouldBe` "2-22:10:02"
      compute "5-20:11:02 - 1-15:03:01" `shouldBe` "4-05:08:01"
    context "when provided with negative input" $ do
      it "yields negative output" $ do
        compute "1" `shouldBe` "1"
        compute "-1" `shouldBe` "-1"
        parseTime [MinusT, TimeT 1] `shouldBe` Just (Neg (DoubleLit 1))
        parseTime [MinusT, OpenT, TimeT 60, MinusT, TimeT 20, CloseT] `shouldBe` Just (Neg (Sub (DoubleLit 60)
                                                                                                (DoubleLit 20)))
      
--vllt hilft das?
--http://www.engr.mun.ca/~theo/Misc/exp_parsing.htm#classic
