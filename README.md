# time calculator

A shell for simple calculations with time.  
If your system has GNU Readline please consider the `readline` branch which let's you scroll through history.

## Use cases

1)
The last job started at 2018-06-28 13:38:18 and ended at 2018-06-30 01:57:40.
If I start a new job now (17:36) when will I have results?

```
> timecalc "17:36:00 + 30-01:57:40 - 28-13:38:18"
> 2-05:55:22
```

So, the day after tomorrow at about 6am.


2)
This youtube video takes 26:30, how long will it take at 1.25 speed?
```
> timecalc "26:30 / 1.25"
> 21:12
```

3)
for shell, call without arguments

4)
input formats
```
d-hh:mm:ss.miliseconds
hh:mm:ss.miliseconds
mm:ss.miliseconds
ss.miliseconds
1d+4s
#overflows are possible
1:100:60 -> 2:41:00
```

## installation

```
#clone this repository
#cd into it
#stack build
execpath=$(stack exec -- whereis time-calculator-exe | awk '{print $2}') #path is 2nd word of output
sudo ln -s $execpath /usr/local/bin/timecalc
```